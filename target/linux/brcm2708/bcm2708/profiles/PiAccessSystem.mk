#
# Copyright (C) 2015 OpenWrt.org
#
# This is free software, licensed under the GNU General Public License v2.
# See /LICENSE for more information.
#

define Profile/AccessPiThinClient
  NAME:=Raspberry Pi B/B+ Access System (ramdisk-based)
  FEATURES:=ramdisk ext4
  PACKAGES:=nfc-security-frontend nfc-security-display libfreefare-examples \
	nfc-utils mfoc -odhcpd curl
endef
define Profile/AccessPiThinClient/Description
  Raspberry Pi as an access system client. This profile boots into an initramfs-system
endef

define Profile/AccessPiClient
 NAME:=Raspberry Pi B/B+ Access System (normal)
endef
define Profile/AccessPiClient/Description
  Raspberry Pi as an access system client. This profile has all the required components for a client, boots from ext4
endef

$(eval $(call Profile,AccessPiThinClient))
$(eval $(call Profile,AccessPiClient))
